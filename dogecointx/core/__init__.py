from typing import List, cast

from bitcointx.core import (
    CoreCoinClassDispatcher, CoreCoinClass,

    CTransaction, CTxIn, CTxOut, CTxWitness, CTxInWitness,
    CMutableTransaction, CMutableTxIn, CMutableTxOut, CMutableTxWitness,
    CMutableTxInWitness, COutPoint, CMutableOutPoint
)
from .script import ScriptDogecoinClassDispatcher


class CoreDogecoinClassDispatcher(CoreCoinClassDispatcher,
                                  depends=[ScriptDogecoinClassDispatcher]):
    ...


class CoreDogecoinClass(CoreCoinClass, metaclass=CoreDogecoinClassDispatcher):
    ...


class CDogecoinOutPoint(COutPoint, CoreDogecoinClass):
    """Dogecoin COutPoint"""
    __slots__: List[str] = []


class CDogecoinMutableOutPoint(CDogecoinOutPoint, CMutableOutPoint,
                               CoreDogecoinClass, mutable_of=CDogecoinOutPoint):
    """A mutable Dogecoin COutPoint"""
    __slots__: List[str] = []


class CDogecoinTxIn(CTxIn, CoreDogecoinClass):
    """An immutable Dogecoin TxIn"""
    __slots__: List[str] = []


class CDogecoinMutableTxIn(CDogecoinTxIn, CMutableTxIn, CoreDogecoinClass,
                           mutable_of=CDogecoinTxIn):
    """A mutable Dogecoin TxIn"""
    __slots__: List[str] = []


class CDogecoinTxOut(CTxOut, CoreDogecoinClass):
    """A immutable Dogecoin TxOut"""
    __slots__: List[str] = []


class CDogecoinMutableTxOut(CDogecoinTxOut, CMutableTxOut, CoreDogecoinClass,
                            mutable_of=CDogecoinTxOut):
    """A mutable Dogecoin CTxOut"""
    __slots__: List[str] = []


class CDogecoinTxInWitness(CTxInWitness, CoreDogecoinClass):
    """Immutable Dogecoin witness data for a single transaction input"""
    __slots__: List[str] = []


class CDogecoinMutableTxInWitness(CDogecoinTxInWitness, CMutableTxInWitness,
                                  CoreDogecoinClass,
                                  mutable_of=CDogecoinTxInWitness):
    """Mutable Dogecoin witness data for a single transaction input"""
    __slots__: List[str] = []


class CDogecoinTxWitness(CTxWitness, CoreDogecoinClass):
    """Immutable witness data for all inputs to a transaction"""
    __slots__: List[str] = []


class CDogecoinMutableTxWitness(CDogecoinTxWitness, CMutableTxWitness,
                                CoreDogecoinClass,
                                mutable_of=CDogecoinTxWitness):
    """Witness data for all inputs to a transaction, mutable version"""
    __slots__: List[str] = []


class CDogecoinTransaction(CTransaction, CoreDogecoinClass):
    """Dogecoin transaction, mutable version"""
    __slots__: List[str] = []

    def to_mutable(self) -> 'CDogecoinMutableTransaction':
        return cast('CDogecoinMutableTransaction', super().to_mutable())

    def to_immutable(self) -> 'CDogecoinTransaction':
        return cast('CDogecoinTransaction', super().to_immutable())


class CDogecoinMutableTransaction(CDogecoinTransaction, CMutableTransaction,
                                  CoreDogecoinClass,
                                  mutable_of=CDogecoinTransaction):
    """Dogecoin transaction"""
    __slots__: List[str] = []


__all__ = (
    'CDogecoinOutPoint',
    'CDogecoinMutableOutPoint',
    'CDogecoinTxIn',
    'CDogecoinMutableTxIn',
    'CDogecoinTxOut',
    'CDogecoinMutableTxOut',
    'CDogecoinTransaction',
    'CDogecoinMutableTransaction',
    'CDogecoinTxWitness',
    'CDogecoinMutableTxWitness',
    'CDogecoinMutableTxInWitness',
    'CDogecoinTxInWitness',
)
